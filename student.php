<?php
    $khoa = array(
        '' => '',
        "MAT" => 'Khoa học máy tính',
        "KDL" => 'khoa học vật liệu'
    );

$pdo = new PDO('mysql:host=localhost;port=3306;dbname=test','root', ''); 
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$faculty = $_GET['department'] ?? null;
$keyWord = $_GET['keyWord'] ?? null;
echo $keyWord;
echo $faculty;

if ($faculty!=null && $keyWord!=null) {
    $statement = $pdo-> prepare('SELECT * FROM student WHERE faculty like :faculty and (name like :keyWord or address like :keyWord) ORDER BY id ASC');
    $statement-> bindValue(':faculty', "%$faculty%");
    $statement-> bindValue(':keyWord', "%$keyWord%");
} else if ($faculty!=null) {
    $statement = $pdo-> prepare('SELECT * FROM student WHERE faculty like :faculty ORDER BY id ASC');
    $statement-> bindValue(':faculty', "%$faculty%");
} else if ($keyWord!=null) {
    $statement = $pdo-> prepare('SELECT * FROM student WHERE name like :keyWord or address like :keyWord ORDER BY id ASC');
    $statement-> bindValue(':keyWord', "%$keyWord%");
} else {
    $statement= $pdo-> prepare('SELECT * FROM student ORDER BY id ASC');
}
// $statement=$pdo-> prepare('SELECT * FROM student ORDER BY id ASC');
$statement->execute();
$students = $statement->fetchAll(PDO::FETCH_ASSOC);

?>
<html>

<head>
    <title>Trang đăng nhập</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="lab07.css">
</head>

<body>
    <div class="mx-auto mt-5 border d-flex flex-column" style="max-width: 700px; border: 2px solid #2980d7 !important">
        <div class="my-3 d-flex flex-column mx-auto">
            <form style="position: center !important; width:300px;">
                <div class="searchDiv">
                    <label class="label">Khoa</label>
                    <div class="fillInfo">
                        <select class="inp" name="department" style="width: 189px; height:100%; border: 1.5px solid #58769a; background:#e1eaf4
">
                            <?php foreach ($khoa as $key => $i) : ?>
                            <option value=<?php echo $key ?>><?php echo $i ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="d-flex mb-2" style="height: 35px">
                    <label class="input-label h-100 mr-3">Từ khóa</label>
                    <div class="fillInfo">
                        <input class="h-100" type="text" style="width: 189px; height:100%; border: 1.5px solid #58769a;background:#e1eaf4" name="keyWord">
                    </div>
                </div>
                <div class="d-flex">
                    <button type="submit" class="btn1 mx-auto btn btn-primary btn-submit mt-3">Tìm kiếm</button>
                </div>
            </form>
        </div>
        <div style="display:flex;margin-right:10px;margin-left:10px">
            <span style="font-weight: 500">Số sinh viên tìm thấy : <?php echo count($students) ?></span>
            <div style="margin-left:auto; margin-right:40px">
                <a href="form.php" class="btn1 btn btn-primary btn-add">Thêm</a>
            </div>
        </div>
        <div class="mt-3">
        <table>
        <thead>
        <tr>
        <th scope="col">id</th>
        <th scope="col">name</th>
        <th scope="col">address</th>
        <th scope="col">Action</th>
        </tr>
    </thead>
                <tbody>
                <?php foreach ($students as $i => $student): ?>
                <tr>
                <td><?php echo $student['id'] ?></td>
                <td><?php echo $student['name'] ?></td>
                <td><?php echo $student['address'] ?></td>
                <td>
                    <div style="display: flex;">
                        <button  class="btn2 btn btn-action">Xóa</button>
                        <button class="btn2 btn btn-action">Sửa</button>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
